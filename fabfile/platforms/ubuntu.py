from __future__ import unicode_literals

from .debian import restart_supervisor, restart_webserver


def post_install():
    """
    Post install operations on an Ubuntu system
    """    
    restart_supervisor()
    restart_webserver()
