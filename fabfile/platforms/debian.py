from __future__ import unicode_literals

import os

from fabric.api import cd, env, run, settings, sudo, task
from fabric.contrib.files import upload_template

from ..literals import DB_MYSQL, DB_PGSQL, WEB_APACHE, WEB_NGINX


def install_dependencies():
    """
    Install Debian dependencies
    """

    sudo('apt-get update')
    sudo(
        'apt-get install -y gcc ghostscript gpgv libjpeg-dev libpng-dev libtiff-dev libmagic1 python-virtualenv poppler-utils python-dev tesseract-ocr unpaper'
    )


def install_database_manager():
    """
    Install the database manager on an Ubuntu system
    """

    if env.database_manager == DB_MYSQL:
        sudo('apt-get install -y mysql-server libmysqlclient-dev')
        
        with cd(env.virtualenv_path):
            sudo('source bin/activate; pip install MySQL-python')

    elif env.database_manager == DB_PGSQL:
        sudo('apt-get install -y postgresql libpq-dev')
        
        with cd(env.virtualenv_path):
            sudo('source bin/activate; pip install psycopg2')


def install_webserver():
    """
    Installing the Debian packages for the webserver
    """
   
    if env.webserver == WEB_APACHE:
        sudo('apt-get install -y apache2 libapache2-mod-wsgi')
        
        with settings(warn_only=True):
            # Get rid of Apache's default site
            sudo('a2dissite default')

    if env.webserver == WEB_NGINX:
        sudo('apt-get install -y nginx')
        
        with settings(warn_only=True):
            # Get rid of Apache's default site
            sudo('rm /etc/nginx/sites-enabled/default')

        with cd(env.virtualenv_path):
            sudo('source bin/activate; pip install uwsgi')

        upload_template(
            filename=os.path.join('fabfile', 'templates', 'uwsgi', 'uwsgi.ini'),
            destination=env.install_path, context=env, use_sudo=True
        )
        
        with settings(warn_only=True):
            sudo('mkdir /var/log/uwsgi')

        upload_template(
            filename=os.path.join('fabfile', 'templates', 'nginx', 'site'),
            destination='/etc/nginx/sites-available/mayan', context=env, use_sudo=True
        )

        sudo('ln -sf /etc/nginx/sites-available/mayan /etc/nginx/sites-enabled/')


def install_supervisor():
    """
    Installing the Debian packages for the supervisor
    """
   
    sudo('apt-get install -y supervisor')
    
    with settings(warn_only=True):
        sudo('mkdir /var/log/mayan')

    upload_template(
        filename=os.path.join('fabfile', 'templates', 'supervisor', 'mayan-uwsgi'),
        destination='/etc/supervisor/conf.d/mayan-uwsgi.conf', context=env, use_sudo=True
    )

    upload_template(
        filename=os.path.join('fabfile', 'templates', 'supervisor', 'mayan-celery'),
        destination='/etc/supervisor/conf.d/mayan-celery.conf', context=env, use_sudo=True
    )


def install_redis():
    """
    Installing the Debian packages for redis
    """
   
    sudo('apt-get install -y redis-server')
    
    with cd(env.virtualenv_path):
        sudo('source bin/activate; pip install redis')

    upload_template(
        filename=os.path.join('fabfile', 'templates', 'celery', 'broker'),
        destination=env.mayan_settings_path, context=env, use_sudo=True
    )


def restart_supervisor():
    """
    Restart the process supervisor
    """
    sudo('/etc/init.d/supervisor restart')

    
def restart_webserver():
    """
    Restart the webserver
    """
    sudo('/etc/init.d/nginx restart')


def fix_permissions():
    """
    Fix installation files' permissions on a Debian system
    """
    sudo('chmod 770 %s -R' % env.virtualenv_path)
    sudo('chgrp www-data %s -R' % env.virtualenv_path)


def post_install():
    """
    Post install operations on a Debian system
    """    
    restart_supervisor()
    restart_webserver()

