from __future__ import unicode_literals

import sys

from fabric.api import task, env
from fabric.colors import white

import databases
import mayan_edms
import platforms
import webservers

from .conf import print_supported_configs
from .server_config import servers

print(white('''
 __  __                           _____ ____  __  __ ____  
|  \/  | __ _ _   _  __ _ _ __   | ____|  _ \|  \/  / ___| 
| |\/| |/ _` | | | |/ _` | '_ \  |  _| | | | | |\/| \___ \ 
| |  | | (_| | |_| | (_| | | | | | |___| |_| | |  | |___) |
|_|  |_|\__,_|\__, |\__,_|_| |_| |_____|____/|_|  |_|____/ 
              |___/             
''', bold=True))

print(white('Mayan EDMS Fabric installation file\n', bold=True))

print_supported_configs()


@task
def install():
    """
    Perform a complete install of Mayan EDMS on a host
    """
    platforms.install_dependencies()
    platforms.install_mayan()
    mayan_edms.base_config()
    platforms.install_database_manager()
    databases.create_user()
    databases.create_database()
    mayan_edms.database_config()
    platforms.install_redis()
    mayan_edms.celery_config()
    mayan_edms.initial_setup()
    mayan_edms.collect_static()
    platforms.fix_permissions()
    platforms.install_webserver()
    platforms.install_supervisor()
    webservers.install_site()
    webservers.restart()
    platforms.post_install()

'''
# Disabled until properly implemented
@task
def upgrade():
    """
    Perform a Mayan EDMS installation upgrade
    """
    mayan_edms.upgrade()
'''

@task
def uninstall():
    """
    Perform a complete removal of Mayan EDMS from a host
    """
    platforms.delete_mayan()
    webservers.remove_site()
    webservers.restart()

    if env.drop_database:
        databases.drop_database()
        databases.drop_user()
