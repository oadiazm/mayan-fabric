from __future__ import unicode_literals

import os

from fabric.api import cd, env, task, sudo
from fabric.contrib.files import upload_template

from ..conf import setup_environment


@task
def base_config():
    """
    Create base settings/local.py file
    """
    setup_environment()
    with cd(env.virtualenv_path):
        sudo(
            'source bin/activate; mayan-edms.py createsettings'
        )

@task
def database_config():
    """
    Tailor settings/local.py file to the database manager selected
    """
    setup_environment()
    upload_template(
        filename=os.path.join('fabfile', 'templates', 'django', 'database'),
        destination=env.mayan_settings_path, context=env, use_sudo=True
    )


@task
def celery_config():
    """
    Tailor settings/local.py file to the task queue manager selected
    """
    setup_environment()
    upload_template(
        filename=os.path.join('fabfile', 'templates', 'celery', 'broker'),
        destination=env.mayan_settings_path, context=env, use_sudo=True
    )
    
    
@task
def initial_setup():
    """
    Setup Mayan EDMS for use
    """
    setup_environment()
    with cd(env.mayan_settings_path):
        sudo(
            'cat database >> local.py'
        )

        sudo(
            'cat broker >> local.py'
        )

    with cd(env.virtualenv_path):        
        sudo(
            'source bin/activate; mayan-edms.py initialsetup'
        )


@task
def collect_static():
    """
    Perform Django's collectstatic command
    """
    setup_environment()
    with cd(env.virtualenv_path):
        sudo(
            'source bin/activate; mayan-edms.py collectstatic --noinput' % (env)
        )
