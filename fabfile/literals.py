from __future__ import unicode_literals

OS_UBUNTU = 'ubuntu'
OS_REDHAT = 'redhat'
OS_CENTOS = 'centos'
OS_FEDORA = 'fedora'
OS_WINDOWS = 'windows'
OS_FREEBSD = 'freebds'
OS_DEBIAN = 'debian'

OS_CHOICES = {
    OS_UBUNTU: 'Ubuntu',
    OS_DEBIAN: 'Debian',
}

DEFAULT_INSTALL_PATH = {
    OS_UBUNTU: '/usr/share/mayan-edms',
    OS_DEBIAN: '/usr/share/mayan-edms',
}

DB_MYSQL = 'mysql'
DB_PGSQL = 'pgsql'
DB_SQLITE = 'sqlite'
DB_ORACLE = 'oracle'

DB_CHOICES = {
    DB_PGSQL: 'PostgreSQL',
}

DJANGO_DB_DRIVERS = {
    DB_MYSQL: 'mysql',
    DB_PGSQL: 'postgresql_psycopg2',
    DB_SQLITE: 'sqlite3',
    DB_ORACLE: 'oracle',
}

WEB_APACHE = 'apache'
WEB_NGINX = 'nginx'

WEB_CHOICES = {
    WEB_NGINX: 'Nginx',
}

DEFAULT_OS = OS_UBUNTU
DEFAULT_DATABASE_MANAGER = DB_PGSQL
DEFAULT_DATABASE_NAME = 'mayan'
DEFAULT_WEBSERVER = WEB_NGINX
DEFAULT_DATABASE_USERNAME = 'mayan'
DEFAULT_DATABASE_HOST = '127.0.0.1'
DEFAULT_PASSWORD_LENGTH = 10
